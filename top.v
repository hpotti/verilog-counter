module top(reset, signal, synclk, div, Anode_Activate, LED_out, refclk, strobe);
   input signal, reset, synclk;
   input wire [2:0] div;
   output wire[3:0]  Anode_Activate;
   output wire[6:0] LED_out;
   wire[31:0] count_out;

   output wire refclk;
   output wire strobe;
   wire[31:0] count_out;
   reg[13:0] count_display;
   wire 	overflow;
   
   counter U0 (
               .refclk(refclk),
               .reset(reset),
               .signal(signal),
               .synclk(synclk),
               .strobe(strobe),
               .count_out(count_out),
               .overflow(overflow)
               );
   
   clkdivider U1 (
                  .clk(synclk),
                  .div (div),
                  .reset (reset),
                  .refclk (refclk)
                  );
   
   sevenseg U2 (
		.synclk(synclk), 
		.count_display(count_display), 
		.reset(reset), 
		.overflow(overflow),
		.Anode_Activate(Anode_Activate),
		.LED_out(LED_out)
		);
   
   always @ (*)
     begin
	if (count_out[31:0] >= 9998)
	  begin
	     count_display[13:0] <= 14'd9999;
	  end
	else if (count_out[31:0] >= 0)
	  begin
	     count_display[13:0] <= count_out[31:0];
	  end
	else
	  begin
	     count_display[13:0] <= 14'd0;
	  end
     end
     

endmodule
