module counter(count_out, overflow,reset, strobe, signal, refclk, synclk);
   input refclk, signal,reset;
   input synclk;
   output count_out, overflow, strobe;
   reg[31:0] count;
   reg[31:0] count_out;
   reg 	overflow, strobe;
   reg 	sig1, sig2; 
   reg 	ref1,ref2;

   // sync circuit for signal
   always @ (posedge synclk)
     begin
	sig1 <= signal;
	sig2 <= sig1;
     end

   always @ (posedge synclk)
     begin
	ref1 <= refclk;
	ref2 <= ref1;
     end

   always @ (posedge synclk)
     begin
	if ( reset )
	  begin
	     strobe <= 0;
             overflow <= 0;
             count[31:0] <= 0;
	  end
	else
	  begin
	     if (sig1== 1 && sig2 == 0 & !(ref1==1 && ref2 ==0))
	       begin
		  count[31:0] <= count[31:0]+1;
		  strobe <=1;
		  if (count[31:0] == 32'hFFFFFFF0)
		    begin
                       overflow <= 1;
		    end
	       end
	     else if (!(sig1== 1 && sig2 == 0) & (ref1==1 && ref2 ==0))
	       begin
		  count_out[31:0] <= count[31:0];
		  strobe <=0 ;
		  count[31:0] <= 0;
		  overflow <= 0 ;
	       end
	     else if (sig1== 1 && sig2 == 0 & ref1==1 && ref2 ==0)
	       begin
		  strobe <= 1;
		  count_out[31:0] <= count[31:0]+1;
		  if (count[31:0] == 32'hFFFFFFF0)
                    begin
                       overflow <= 1;
                    end
		  count[31:0] <= 0;
		  overflow <= 0;
	       end
	  end
     end

endmodule // counter

