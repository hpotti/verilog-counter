# Clock signal
set_property PACKAGE_PIN W5 [get_ports synclk]       
 set_property IOSTANDARD LVCMOS33 [get_ports synclk]
 create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports synclk]

#sw[15]
set_property PACKAGE_PIN R2 [get_ports reset]     
 set_property IOSTANDARD LVCMOS33 [get_ports reset]

##Sch name = JA1
set_property PACKAGE_PIN J1 [get_ports {refclk}]
        set_property IOSTANDARD LVCMOS33 [get_ports {refclk}]

##Sch name = JA2
set_property PACKAGE_PIN L2 [get_ports {strobe}]
	        set_property IOSTANDARD LVCMOS33 [get_ports {strobe}]

##Sch name = JXADC
##Sch name = XA1_P
set_property PACKAGE_PIN J3 [get_ports {signal}]
        set_property IOSTANDARD LVCMOS33 [get_ports {signal}]
##Sch name = XA2_P
set_property PACKAGE_PIN L3 [get_ports {div[0]}]
        set_property IOSTANDARD LVCMOS33 [get_ports {div[0]}]
##Sch name = XA3_P
set_property PACKAGE_PIN M2 [get_ports {div[1]}]
        set_property IOSTANDARD LVCMOS33 [get_ports {div[1]}]
##Sch name = XA4_P
set_property PACKAGE_PIN N2 [get_ports {div[2]}]
        set_property IOSTANDARD LVCMOS33 [get_ports {div[2]}]
##Sch name = XA1_N
#set_property PACKAGE_PIN K3 [get_ports {JXADC[4]}]
        #set_property IOSTANDARD LVCMOS33 [get_ports {JXADC[4]}]
##Sch name = XA2_N
#set_property PACKAGE_PIN M3 [get_ports {JXADC[5]}]
        #set_property IOSTANDARD LVCMOS33 [get_ports {JXADC[5]}]
##Sch name = XA3_N
#set_property PACKAGE_PIN M1 [get_ports {JXADC[6]}]
        #set_property IOSTANDARD LVCMOS33 [get_ports {JXADC[6]}]
##Sch name = XA4_N
#set_property PACKAGE_PIN N1 [get_ports {JXADC[7]}]
        #set_property IOSTANDARD LVCMOS33 [get_ports {JXADC[7]}]

#LED0
set_property PACKAGE_PIN U16 [get_ports {ofled}]
        set_property IOSTANDARD LVCMOS33 [get_ports {ofled}]


#seven-segment LED display
set_property PACKAGE_PIN W7 [get_ports {LED_out[6]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[6]}]
set_property PACKAGE_PIN W6 [get_ports {LED_out[5]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[5]}]
set_property PACKAGE_PIN U8 [get_ports {LED_out[4]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[4]}]
set_property PACKAGE_PIN V8 [get_ports {LED_out[3]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[3]}]
set_property PACKAGE_PIN U5 [get_ports {LED_out[2]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[2]}]
set_property PACKAGE_PIN V5 [get_ports {LED_out[1]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[1]}]
set_property PACKAGE_PIN U7 [get_ports {LED_out[0]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {LED_out[0]}]
set_property PACKAGE_PIN U2 [get_ports {Anode_Activate[0]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {Anode_Activate[0]}]
set_property PACKAGE_PIN U4 [get_ports {Anode_Activate[1]}]                    
   set_property IOSTANDARD LVCMOS33 [get_ports {Anode_Activate[1]}]
set_property PACKAGE_PIN V4 [get_ports {Anode_Activate[2]}]               
   set_property IOSTANDARD LVCMOS33 [get_ports {Anode_Activate[2]}]
set_property PACKAGE_PIN W4 [get_ports {Anode_Activate[3]}]          
   set_property IOSTANDARD LVCMOS33 [get_ports {Anode_Activate[3]}]
