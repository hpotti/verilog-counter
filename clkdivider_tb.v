`timescale 1 ns / 1 ns

module clkdivider_tb;
   reg clk, reset;
   reg[2:0] div;
   wire refclk;

   clkdivider U0 (
		  .clk (clk),
		  .div (div),
		  .reset (reset),
		  .refclk (refclk)
		  );

   initial begin
      clk = 0;
      reset = 0;
      div = 3'd7;
      #10;
      reset = 1;
      #10;
      reset = 0;
   end

always 
  #5 clk = !clk;
endmodule
      

      
     
   
