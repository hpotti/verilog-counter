module clkdivider (div, clk, reset, refclk);
   input clk, reset;
   input wire[2:0] div;
   output refclk;
   reg refclk;
   reg[26:0] count;
   reg[25:0] ndiv;
   
always @ (*) begin
case (div)
  3'd0: ndiv[25:0] = 26'd50000000;
  3'd1: ndiv[25:0] = 26'd5000000;
  3'd2: ndiv[25:0] = 26'd500000;
  3'd3: ndiv[25:0] = 26'd50000;
  3'd4: ndiv[25:0] = 26'd5000;
  3'd5: ndiv[25:0] = 26'd500;
  3'd6: ndiv[25:0] = 26'd50;
  3'd7: ndiv[25:0] = 26'd5;
  default: ndiv[25:0] = 26'd500;
endcase
end
     
always @ (posedge clk) 
begin
    if (reset) 
    begin
        count[26:0] <= 27'h0000000;
        refclk <=0;
     end 
    else 
      begin
	 
        if (count[26:0] == ndiv[25:0])
	  begin
	     refclk <= 0;
	     count[26:0] <= count[26:0] + 27'h0000001;
	  end
        else if (count[26:0] == ndiv[25:0]*2) 
        begin
	    refclk <= 1;
	    count[26:0] <=27'h0000000;
	end
	else
	  begin
	     count[26:0] <= count[26:0] + 27'h0000001;
	  end
     end
end
endmodule // clkdivider
