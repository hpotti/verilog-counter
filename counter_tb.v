`timescale 1 ns / 1 ns

module counter_tb;
   reg signal,synclk, reset;
   wire refclk;
   wire overflow, strobe;
   wire[31:0] count_out;
   reg[2:0] div;
   

   counter U0 (
	       .refclk(refclk),
	       .reset(reset),
	       .signal(signal),
	       .synclk(synclk),
	       .strobe(strobe),
	       .count_out(count_out),
	       .overflow(overflow)
	       );
   clkdivider U1 (
		  .clk(synclk),
                  .div (div),
                  .reset (reset),
                  .refclk (refclk)
		  );
   
   initial begin
      synclk = 0;
      count_out[31:0] = 32'd0;
      signal = 0;
      reset = 0;
      div = 3'd4;
      #10
	reset = 1;
      #10
	reset = 0;
   end

   always
     #5 synclk = !synclk;

   always
     #500 signal = !signal;
   
   
endmodule 
